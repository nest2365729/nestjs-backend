import { UsersEvent } from '@app/microservices/apps/users/users-event';

export class UserUpdatedEvent {
  static type: UsersEvent.USER_UPDATED;
  id: string;
  departmentId: string;
  constructor({ id, departmentId }: { id: string; departmentId: string }) {
    this.id = id;
    this.departmentId = departmentId;
  }
}
