export class UserDeleteCommand {
  constructor({ id, departmentId }: { id: string; departmentId: string }) {
    this.id = id;
    this.departmentId = departmentId;
  }
  id: string;
  departmentId: string;
}
