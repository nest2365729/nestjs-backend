export class UserUpdateCommand {
  constructor({
    id,
    email,
    username,
    departmentId,
  }: {
    id: string;
    email: string;
    username: string;
    departmentId: string;
  }) {
    this.id = id;
    this.email = email;
    this.username = username;
    this.departmentId = departmentId;
  }
  id: string;
  email: string;
  username: string;
  departmentId: string;
}
