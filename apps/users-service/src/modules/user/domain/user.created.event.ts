import { UsersEvent } from '@app/microservices/apps/users/users-event';

export class UserCreatedEvent {
  static type: UsersEvent.USER_CREATED;
  id: string;
  departmentId: string;
  constructor({ id, departmentId }: { id: string; departmentId: string }) {
    this.id = id;
    this.departmentId = departmentId;
  }
}
