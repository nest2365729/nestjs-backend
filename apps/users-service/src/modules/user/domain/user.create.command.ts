export class UserCreateCommand {
  constructor({
    email,
    username,
    departmentId,
  }: {
    email: string;
    username: string;
    departmentId: string;
  }) {
    this.email = email;
    this.username = username;
    this.departmentId = departmentId;
  }
  email: string;
  username: string;
  departmentId: string;
}
