import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { RpcException } from '@nestjs/microservices';

import { ServiceType } from '@app/microservices/const/services';
import { UsersEvent } from '@app/microservices/apps/users/users-event';
import { UserCreatedEvent } from '../domain/user.created.event';
import { UserCreateCommand } from '../domain/user.create.command';
import { UserRepository } from '../database/user.repository';

@Injectable()
export class UserCreateHandler {
  constructor(
    private readonly userRepository: UserRepository,
    @Inject(ServiceType.RABBIT_MQ) private readonly client: ClientProxy,
    protected readonly logger: Logger,
  ) {
    this.logger = new Logger(UserCreateHandler.name);
  }

  async execute(command: UserCreateCommand): Promise<{ id: string }> {
    this.logger.debug(command, `Processing command <UserCreateCommand>.`);

    const exist = await this.userRepository.findOne({
      where: [
        { username: command.username },
        {
          email: command.email,
        },
      ],
    });

    if (exist) {
      throw new RpcException({
        statusCode: 409,
        message: `User with given username ${command.username} and email ${command.email} already exists.`,
      });
    }

    const user = this.userRepository.create(command);

    await this.userRepository.save(user);

    this.client.emit<UsersEvent, { id: string; departmentId: string }>(
      UsersEvent.USER_CREATED,
      new UserCreatedEvent({
        id: user.id,
        departmentId: user.departmentId,
      }),
    );

    return { id: user.id };
  }
}
