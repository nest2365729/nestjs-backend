import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { RpcException } from '@nestjs/microservices';
import { isUUID } from 'class-validator';

import { ServiceType } from '@app/microservices/const/services';
import { UsersEvent } from '@app/microservices/apps/users/users-event';
import { UserRepository } from '../database/user.repository';
import { UserDeleteCommand } from '../domain/user.delete.command';
import { UserDeletedEvent } from '../domain/user.deleted.event';

@Injectable()
export class UserDeleteHandler {
  constructor(
    private readonly userRepository: UserRepository,
    @Inject(ServiceType.RABBIT_MQ) private readonly client: ClientProxy,
    protected readonly logger: Logger,
  ) {
    this.logger = new Logger(UserDeleteHandler.name);
  }

  async execute(command: UserDeleteCommand): Promise<void> {
    this.logger.debug(command, `Processing command <UserDeleteCommand>.`);

    if (!isUUID(command.id)) {
      throw new RpcException({
        statusCode: 400,
        message: `ID: ${command.id} is not a valid UUID`,
      });
    }

    const user = await this.userRepository.findOne({
      where: { id: command.id },
    });
    if (!user) {
      throw new RpcException({
        statusCode: 404,
        message: 'User not found!',
      });
    }

    await this.userRepository.delete(user.id);
    this.client.emit<UsersEvent, { id: string; departmentId: string }>(
      UsersEvent.USER_UPDATED,
      new UserDeletedEvent({
        id: user.id,
        departmentId: user.departmentId,
      }),
    );
    return;
  }
}
