import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { RpcException } from '@nestjs/microservices';
import { isUUID } from 'class-validator';

import { ServiceType } from '@app/microservices/const/services';
import { UsersEvent } from '@app/microservices/apps/users/users-event';
import { UserRepository } from '../database/user.repository';
import { UserUpdateCommand } from '../domain/user.update.command';
import { UserUpdatedEvent } from '../domain/user.updated.event';

@Injectable()
export class UserUpdateHandler {
  constructor(
    private readonly userRepository: UserRepository,
    @Inject(ServiceType.RABBIT_MQ) private readonly client: ClientProxy,
    protected readonly logger: Logger,
  ) {
    this.logger = new Logger(UserUpdateHandler.name);
  }

  async execute(command: UserUpdateCommand): Promise<void> {
    this.logger.debug(command, `Processing command <UserUpdateCommand>.`);

    if (!isUUID(command.id)) {
      throw new RpcException({
        statusCode: 400,
        message: `ID: ${command.id} is not a valid UUID`,
      });
    }

    const user = await this.userRepository.findOne({
      where: { id: command.id },
    });
    if (!user) {
      throw new RpcException({
        statusCode: 404,
        message: 'User not found!',
      });
    }

    user.email = command.email ?? user.email;
    user.username = command.username ?? user.username;
    user.departmentId = command.departmentId ?? user.departmentId;
    user.updatedAt = new Date();

    await this.userRepository.update(user.id, user);
    this.client.emit<UsersEvent, { id: string; departmentId: string }>(
      UsersEvent.USER_UPDATED,
      new UserUpdatedEvent({
        id: user.id,
        departmentId: user.departmentId,
      }),
    );
    return;
  }
}
