import { UserCreateCommand } from '../domain/user.create.command';
import { UserUpdateCommand } from '../domain/user.update.command';
import { UserDeleteCommand } from '../domain/user.delete.command';
import { UserCreateHandler } from '../application/user.create.handler';
import { UserUpdateHandler } from '../application/user.update.handler';
import { UserDeleteHandler } from '../application/user.delete.handler';

export class UsersService {
  constructor(
    private readonly userCreateHandler: UserCreateHandler,
    private readonly userUpdateHandler: UserUpdateHandler,
    private readonly userDeleteHandler: UserDeleteHandler,
  ) {}
  create(command: UserCreateCommand) {
    return this.userCreateHandler.execute(command);
  }

  update(command: UserUpdateCommand) {
    return this.userUpdateHandler.execute(command);
  }

  delete(command: UserDeleteCommand) {
    return this.userDeleteHandler.execute(command);
  }
}
