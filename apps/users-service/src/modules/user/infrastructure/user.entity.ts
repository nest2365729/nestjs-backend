import { IUser } from '@app/microservices/models/user.interface';
import {
  Column,
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class UserEntity implements IUser {
  @PrimaryColumn({ unique: true, type: 'uuid' })
  id: string;

  @Column({ type: 'uuid', nullable: false, name: 'department_id' })
  departmentId: string;

  @Column({ unique: true })
  email: string;

  @Column({ unique: true })
  username: string;

  @CreateDateColumn({ type: 'timestamp without time zone', name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp without time zone', name: 'updated_at' })
  updatedAt: Date;
}
