import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { UsersService } from './users.service';
import { UsersCommand } from '@app/microservices/apps/users/users-command';
import { UserCreateCommand } from '../domain/user.create.command';
import { UserUpdateCommand } from '../domain/user.update.command';
import { UserDeleteCommand } from '../domain/user.delete.command';

@Controller()
export class UsersAppController {
  constructor(private readonly usersService: UsersService) {}

  @MessagePattern({ cmd: UsersCommand.USER_CREATE })
  async create(
    @Payload() { email, username, departmentId }: UserCreateCommand,
  ) {
    return this.usersService.create({
      email,
      username,
      departmentId,
    });
  }

  @MessagePattern({ cmd: UsersCommand.USER_UPDATE })
  async update(
    @Payload()
    { id, username, email, departmentId }: UserUpdateCommand,
  ) {
    return this.usersService.update({
      id,
      email,
      username,
      departmentId,
    });
  }

  @MessagePattern({ cmd: UsersCommand.USER_DELETE })
  async delete(@Payload() { id, departmentId }: UserDeleteCommand) {
    return this.usersService.delete({
      id,
      departmentId,
    });
  }
}
