import { Module } from '@nestjs/common';
import { join } from 'path';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { validationSchema } from './config/validation.schema';
import { DatabaseModule } from './db/database.module';
import { TcpService } from './core/tcp.service';
import { RmqService } from './core/rmq.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './modules/user/infrastructure/user.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ServiceType } from '@app/microservices/const/services';
import { EnvConfig } from './config/environment.config';
import { UsersEvent } from '@app/microservices/apps/users/users-event';

const clients = Object.keys(UsersEvent).map((event) => ({
  name: ServiceType.RABBIT_MQ,
  useFactory: (configService: ConfigService<EnvConfig, true>) => ({
    transport: Transport.RMQ as Transport.RMQ,
    options: {
      urls: [configService.get<string>('RABBIT_MQ_ENDPOINT')],
      queue: event,
      noAck: false,
      persistent: true,
    },
  }),
  inject: [ConfigService],
}));

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: join(process.cwd(), '/apps/users-service/.env'),
      isGlobal: true,
      validationSchema,
    }),
    DatabaseModule,
    TypeOrmModule.forFeature([UserEntity]),
    ClientsModule.registerAsync(clients),
  ],
  controllers: [],
  providers: [TcpService, RmqService],
  exports: [],
})
export class UsersServiceModule {}
