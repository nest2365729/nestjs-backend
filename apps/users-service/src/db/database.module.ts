import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { UserEntity } from '../modules/user/infrastructure/user.entity';

const databaseProvider = {
  provide: 'DATABASE_CONNECTION',
  useFactory: (configService: ConfigService): TypeOrmModuleOptions => ({
    type: 'postgres',
    host: configService.get('DB_HOST'),
    port: +configService.get('DB_PORT'),
    username: configService.get('DB_USERNAME'),
    password: configService.get('DB_PASSWORD'),
    database: configService.get('DB_NAME'),
    entities: [UserEntity], // since only one, we can hardcode it, without specify the path to the entities like *.entity.{js,ts}
    synchronize: true,
  }),
  inject: [ConfigService],
};

@Module({
  imports: [ConfigModule, TypeOrmModule.forRootAsync(databaseProvider)],
})
export class DatabaseModule {}
