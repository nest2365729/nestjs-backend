import { NestFactory } from '@nestjs/core';
import { UsersServiceModule } from './users-service.module';
import { NestLoggerAdapter } from '@app/common/logging';
import { TcpService } from './core/tcp.service';
import { RmqService } from './core/rmq.service';
import { INestApplication } from '@nestjs/common';
import { RmqOptions, TcpOptions } from '@nestjs/microservices';

const connectMicroservice = async (
  app: INestApplication,
  service: TcpOptions | RmqOptions[],
) => {
  (Array.isArray(service) ? service : [service]).map((option) =>
    app.connectMicroservice(option),
  );
};

const startApp = async (): Promise<void> => {
  const app = await NestFactory.create(UsersServiceModule, {
    logger: new NestLoggerAdapter(),
    bufferLogs: true,
  });

  await connectMicroservice(app, app.get<TcpService>(TcpService));
  await connectMicroservice(app, app.get<RmqService>(RmqService));

  await app.startAllMicroservices();
};

startApp().catch((error) => {
  console.error('Error during application bootstrap:', error);
  process.exit(1);
});
