import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TcpOptions, Transport } from '@nestjs/microservices';
import { EnvConfig } from '../config/environment.config';

@Injectable()
export class TcpService implements TcpOptions {
  transport: Transport.TCP;
  options: { host: string; port: number };

  constructor(private readonly configService: ConfigService<EnvConfig>) {
    this.transport = Transport.TCP;
    this.options = {
      host: this.configService.get('USERS_APP_HOST'),
      port: this.configService.get('USERS_APP_PORT'),
    };
  }
}
