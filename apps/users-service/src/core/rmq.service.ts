import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UsersEvent } from '@app/microservices/apps/users/users-event';
import { RmqOptions, Transport } from '@nestjs/microservices';
import { EnvConfig } from '../config/environment.config';

@Injectable()
export class RmqService extends Array<RmqOptions> {
  constructor(private readonly configService: ConfigService<EnvConfig>) {
    super(
      ...Object.keys(UsersEvent).map((key) => ({
        transport: Transport.RMQ as Transport.RMQ,
        options: {
          urls: [configService.get<string>('RABBIT_MQ_ENDPOINT')],
          queue: key as UsersEvent,
          noAck: false,
          persistent: true,
        },
      })),
    );
  }
}
