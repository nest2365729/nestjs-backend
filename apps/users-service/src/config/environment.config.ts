export enum NodeEnvironment {
  Production = 'production',
  Development = 'development',
  Test = 'test',
}

export type DatabaseConfig = {
  DB_NAME: string;
  DB_HOST: string;
  DB_PORT: number;
  DB_USERNAME: string;
  DB_PASSWORD: string;
};

export type UserAppConfig = {
  USERS_APP_HOST: string;
  USERS_APP_PORT: number;
};

export type RabbitMQConfig = {
  RABBIT_MQ_ENDPOINT: string;
};

export type EnvConfig = {
  NODE_ENV: NodeEnvironment;
  PORT: number;
} & DatabaseConfig &
  UserAppConfig &
  RabbitMQConfig;
