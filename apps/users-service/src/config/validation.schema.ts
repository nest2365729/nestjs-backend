import * as Joi from 'joi';
import { EnvConfig, NodeEnvironment } from './environment.config';

const nodeEnvSchema = Joi.string()
  .required()
  .valid(...Object.values(NodeEnvironment));

const portSchema = Joi.number().required();
const dbNameSchema = Joi.string().required();
const dbHostSchema = Joi.string().required();
const dbPortSchema = Joi.number().required();
const dbUsernameSchema = Joi.string().required();
const dbPasswordSchema = Joi.string().required();
const usersAppHostSchema = Joi.string().required();
const usersAppPortSchema = Joi.number().required();
const rabbitMqEndpointSchema = Joi.string().required();

export const validationSchema = Joi.object<EnvConfig>({
  NODE_ENV: nodeEnvSchema,
  PORT: portSchema,
  DB_NAME: dbNameSchema,
  DB_HOST: dbHostSchema,
  DB_PORT: dbPortSchema,
  DB_USERNAME: dbUsernameSchema,
  DB_PASSWORD: dbPasswordSchema,
  USERS_APP_HOST: usersAppHostSchema,
  USERS_APP_PORT: usersAppPortSchema,
  RABBIT_MQ_ENDPOINT: rabbitMqEndpointSchema,
});
