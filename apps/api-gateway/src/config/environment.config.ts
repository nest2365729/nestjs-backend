export enum NodeEnvironment {
  Production = 'production',
  Development = 'development',
  Test = 'test',
}

export type UserAppConfig = {
  USERS_APP_HOST: string;
  USERS_APP_PORT: number;
};

export type DepartmentAppConfig = {
  DEPARTMENTS_APP_HOST: string;
  DEPARTMENTS_APP_PORT: number;
};

export type EnvConfig = {
  NODE_ENV: NodeEnvironment;
  PORT: number;
} & UserAppConfig &
  DepartmentAppConfig;
