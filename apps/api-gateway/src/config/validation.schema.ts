import * as Joi from 'joi';
import { EnvConfig, NodeEnvironment } from './environment.config';

const nodeEnvSchema = Joi.string()
  .required()
  .valid(...Object.values(NodeEnvironment));

const portSchema = Joi.number().required();
const usersAppHostSchema = Joi.string().required();
const usersAppPortSchema = Joi.number().required();
const departmentsAppHostSchema = Joi.string().required();
const departmentsAppPortSchema = Joi.number().required();

export const validationSchema = Joi.object<EnvConfig>({
  NODE_ENV: nodeEnvSchema,
  PORT: portSchema,
  USERS_APP_HOST: usersAppHostSchema,
  USERS_APP_PORT: usersAppPortSchema,
  DEPARTMENTS_APP_HOST: departmentsAppHostSchema,
  DEPARTMENTS_APP_PORT: departmentsAppPortSchema,
});
