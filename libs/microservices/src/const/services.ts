export enum ServiceType {
  API_GATEWAY = 'API_GATEWAY',
  USERS_APP = 'USERS_APP',
  DEPARTMENTS_APP = 'DEPARTMENTS_APP',
  RABBIT_MQ = 'RABBIT_MQ',
}
