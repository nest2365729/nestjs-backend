'use strict';

import { RawAbstractEntity } from '@app/common/abstract.entity';

export class AbstractDto {
  id: string;
  createdAt: Date;
  updatedAt: Date;

  constructor(entity: RawAbstractEntity) {
    this.id = entity.id;
    this.createdAt = entity.createdAt;
    this.updatedAt = entity.updatedAt;
  }
}
