import { LoggerService } from '@nestjs/common'

export class NestLoggerAdapter implements LoggerService {
  getTimestamp(): string {
    return new Date().toISOString()
  }

  log(message: any, ...optionalParams: any[]): void {
    return console.log(`${this.getTimestamp()} ${message}`, optionalParams)
  }

  error(message: any, ...optionalParams: any[]): void {
    return console.error(`${this.getTimestamp()} ${message}`, optionalParams)
  }

  warn(message: any, ...optionalParams: any[]): void {
    return console.warn(`${this.getTimestamp()} ${message}`, optionalParams)
  }

  debug?(message: any, ...optionalParams: any[]): void {
    return console.debug(`${this.getTimestamp()} ${message}`, optionalParams)
  }
}
